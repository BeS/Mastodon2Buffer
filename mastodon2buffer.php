<?php
/**
 * @copyright Copyright (c) 2017 Bjoern Schiessle <bjoern@schiessle.org>
 *
 * @license GNU GPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


require 'bufferApp/buffer.php';
require 'config.php';

class Mastodon2Buffer {

	/** @var bool */
	private $skipReplays;

	/** @var bool */
	private $skipReShares;

	/** @var  string */
	private $atomFeed;

	/** @var string  */
	private $disclaimer;

	/** @var  bool mark all feed entries as processed but don't post them to buffer */
	private $skipAll;

	/** @var  bool output result but don't post to buffer */
	private $dryRun;

	/** @var BufferApp */
	private $buffer;

	/** @var object */
	private $profiles;

	/** @var string pattern to detect re-shares */
	private $reSharePattern = 'shared a status by';

	/** @var  array of already posted items */
	private $history;

	/** @var string the file which stores already posted items */
	private $historyFile = 'history.json';

	/** @var string path to history file */
	private $historyDir;

	/**
	 * Mastodon2Buffer constructor.
	 *
	 * @param array $config
	 */
	public function __construct(array $config, $dryRun, $skipAll) {
		$bufferClientId = $config['bufferClientId'];
		$bufferClientSecret = $config['bufferClientSecret'];
		$bufferAccessToken = $config['bufferAccessToken'];
		$this->skipReplays = $config['skipReplays'];
		$this->skipReShares = $config['skipReShares'];
		$this->atomFeed = $config['atomFeed'];
		$this->disclaimer = $config['disclaimer'];
		$this->skipAll = $skipAll;
		$this->dryRun = $dryRun;

		$home = getenv("HOME");
		$this->historyDir = $home . '/.local/share/Mastodon2Buffer';

		$this->loadHistory();

		$this->buffer = new BufferApp($bufferClientId, $bufferClientSecret, null, $bufferAccessToken);

		if (!$this->buffer->ok) {
			echo "Couldn't connect to buffer\n";
			return;
		} else {
			$this->profiles = $this->buffer->go('/profiles');
		}

		$feed = $this->parseAtomFeed();
		$this->postToBuffer($feed);
		$this->storeHistory();

	}

	private function loadHistory() {

		$this->history = [];

		if (!is_dir($this->historyDir)) {
			mkdir($this->historyDir, 0755, true);
		}

		$path = $this->historyDir . '/' . $this->historyFile;
		if (file_exists($path)) {
			$fileContent = file_get_contents($path);
			$this->history = json_decode($fileContent, true);
		}
	}

	/**
	 * store list of already send messages
	 */
	private function storeHistory() {
		if ($this->dryRun) return;
		while (count($this->history) > 25) {
			array_shift($this->history);
		}

		$path = $this->historyDir . '/' . $this->historyFile;
		$text = json_encode($this->history);
		file_put_contents($path, $text);
	}

	/**
	 * parse atom feed and return array of posts
	 *
	 * @return array
	 */
	public function parseAtomFeed() {
		$result = [];
		$feed = implode(file($this->atomFeed));
		$xml = simplexml_load_string($feed);
		$json = json_encode($xml);
		$array = json_decode($json,true);
		foreach ($array['entry'] as $entry) {
			$plainContent = strip_tags($entry['content']);
			$plainContent = html_entity_decode($plainContent, ENT_QUOTES | ENT_HTML5);
			if (!empty($this->disclaimer)) {
				$plainContent .= "\n\n" . $this->disclaimer . ' ' . $entry['id'];
			}
			if ($this->skipReplays && strpos($plainContent, '@') === 0) {
				continue;
			}
			if ($this->skipReShares && strpos($entry['title'], $this->reSharePattern) !== false) {
				continue;
			}

			$result[hash('sha256', $plainContent)] = $plainContent;
		}
		return array_reverse($result);
	}

	/**
	 * post atom feed to buffer
	 *
	 * @param array $messages
	 */
	private function postToBuffer(array $messages) {
		foreach ($messages as $hash => $message) {
			if (!isset($this->history[$hash])) {
				// only output the message but don't post it
				if ($this->dryRun) {
					echo "\n" . $message . "\n";
					continue;
				}
				$this->history[$hash] = $message;
				// mark as read but don't post
				if ($this->skipAll) {
					continue;
				}

				if (is_array($this->profiles)) {
					foreach ($this->profiles as $profile) {
						// posts go to all accounts enabled by default
						if ($profile->default) {
							$this->buffer->go('/updates/create', array('text' => $message, 'profile_ids[]' => $profile->id, 'now' => true));
						}
					}
				}
			}
		}
	}

}

$dryRun = false;
$skipAll = false;

foreach($argv as $value) {
	if ($value === '-d' || $value === '--dry-run') {
		$dryRun = true;
	}
	if ($value === '-s' || $value === '--skip') {
		$skipAll = true;
	}
	if ($value === '-h' || $value === '--help') {
		echo "\nRun: php mastodon2buffer.php\n\n";
		echo "Optional parameters:\n";
		echo "-s | --skip    : mark all existing feed entries as processed but don't post them to buffer\n";
		echo "-d | --dry-run : don't post anything to buffer, just output what would have been send to buffer\n";
		echo "-h | --help    : output help\n\n";
		exit();
	}
}

$m2b = new Mastodon2Buffer($config, $dryRun, $skipAll);
